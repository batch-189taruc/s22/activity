
/*
    Create functions which can manipulate our arrays.
    */

    let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
    ];
 /*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    

function register(addUser) {
		
		// alreadyRegistered = registeredUsers.every(users => users === addUser);
		alreadyReg = registeredUsers.some(users => users === addUser);

		if(alreadyReg) {
			alert("Registration failed. Username already exists!")
		}

		else {
			registeredUsers.push(addUser)
			alert("Thank you for adding " + addUser)
		} 

		

	}


    let friendsList = [];

 /*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/


function addfriend(addFriend){
	canBeFriends = registeredUsers.some(users => users === addFriend);
	if(canBeFriends){
		friendsList.push(addFriend)
			alert("You have added " + addFriend + "as a friend!")

	}
	else{alert("Adding friend failed. Username does not exist!")}
}


function displayFriends(){
	friendsList.forEach(function(friends){
		if(friends.length = 0){
			alert("You have 0 friends. Add one first.")}

		else{console.log(friends)}
})
}

function displayNumberOfFriends(showFriends){
	numFriends = friendsList.length
	if(numFriends < 1){
		alert("You have 0 friends on your list")
	}else{alert("You have " + numFriends + " on your list");
			console.log()}
	
}

